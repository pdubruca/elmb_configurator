# elmb_configurator



## Description

This script's aim is to reconfigure ADC in ELMBs without the need of third party software. With this script you'll be able to modify:

- Number of ADC channels (Up to 64 channels)
- Polarity (Unipolar or Bipolar)
- ADC conversion rate
- ADC voltage range

## Dependencies

If your using a version lower than Python3.2, make sure to install ArgParse. CanOpen libraries already included when repo is cloned.

This tool is only compatible with CAN interfaces using SocketCan drivers. To communicate with the ELMB make sure to input both the CanBus number and the Node Id as arguments to the command. For more info use ```-h``` as a command argument.

## Usage

Each ADC config allows a set of specific inputs:

### ADC Channels 

Input value is an integer that can range from 1 to 64, depending on desired amount of ADC channels

### Polarity

Input value is either 0, configuring ADC as bipolar or 1, configuring it as unipolar

### ADC conversion rate

Input value is a float specifying the possible ADC conversion rates (Hz) available to ELMB according to Henk's ELMB manual:

- 15
- 30
- 61.6
- 84.5
- 101.1
- 1.88
- 3.76
- 7.51

### ADC Input Voltage Range

Input value is a float specifying the possible ADC input voltage range (V) available to ELMB according to Henk's ELMB manual:

- 0.1
- 0.055
- 0.025
- 1
- 5
- 2.5
